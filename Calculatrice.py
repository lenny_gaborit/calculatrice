import operator as operator
from pile import Maillon, Pile

operations = {
    '+':  operator.add, '-':  operator.sub,
    '*':  operator.mul, '/':  operator.truediv,
    '%':  operator.mod, '**': operator.pow,
    '//': operator.floordiv
}

p = Pile()
début = None
print ("\n Taper fin pour finir le calcul ") 
while not début == "fin" :
        début = input("\n Inseré un chiffre ou une opération :")
        for operateurs in operations.keys():
            if operateurs == début:
                premier = int(p.depiler())
                deuxieme = int(p.depiler())
                rlt = operations[operateurs](premier,deuxieme)
                début = str(rlt)
        p.empiler(début)
        print(p)




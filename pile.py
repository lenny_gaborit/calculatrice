class Maillon():

    def __init__(self, val) -> None:
        self._val = val
        self._suiv = None

    def get_val(self) :
        return self._val 

    def get_suiv(self):
        return self._suiv

    def set_suiv(self, m) -> None:
        if m is None or (type(m._val) == type(self._val)):
            self._suiv = m


    def set_suiv(self, m) -> None:
        if m is None or (type(m._val) == type(self._val)):
            self._suiv = m
        else:
            raise TypeError  

    def __repr__(self) -> str:
        return  f" {self._val}, {self._suiv}"


class Pile():
    
    def __init__(self: "Pile") -> None:
        self._sommet = None 

    def est_vide(self: "Pile") -> bool:
        return self._sommet is None

    def empiler(self: "Pile", val) -> None:
        s: Maillon = Maillon(val)
        if self._sommet is None:
            self._sommet = Maillon(val)
        else:
            s.set_suiv(self._sommet)
            self._sommet = s

    def depiler(self):
        if self._sommet is None:
            raise IndexError("La pile est vide")
        else:
            s = self._sommet.get_val()
            self._sommet = self._sommet.get_suiv()
            return(s)
        

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self._sommet}-|"

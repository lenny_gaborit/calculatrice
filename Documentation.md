# Calculatrice
- Mon **projet** est de créé une Calculatrice comme la Calculatrice HP15C en notation polonaise inversé sur python.
#### Pour cela j'ai utliser deux classe créé en cours:
- La classe **Liste** et la classe **Maillon**
<p>Ces deux classes se trouvent dans le fichier Pile.py du dossier et elles sont importées dans le fichier Calculatrice.py</p>

#### J'ai ensuite donc créé un deuxieme fichier *Calculatrice.py* dans lequel le programme s'éxecute.
- Après avoir importé les classes Pile et Maillon et aussi le module operator je créé un dictionnaire Opération qui assossie à chaque operation une fonction du module operator
- J'ai ensuite créé une classe Pile p et une variable début qui correspondra à la valeur rentrée dans la pile à chaque étape
- Mon programme s'arrête lorsque que l'on rentre **fin** dans la variable début
- Pour résumer l'utilisateur va rentrer un chiffre ou une opérations qui va être ajouté à la pile et lorsqu'il ajoute une operation on *depile* deux fois et on associe chaque valeur aux variables **premier** et **deuxieme** et on effectue l'opération puis on *empile*  le résultat dans la pile.

#### Utilisation 
- Il faut d'abord télécharger les deux autres fichiers du dossier et les mettres sur le bureau ou dans un même dossier 
- Puis **ouvrir** le fichier Calculatrice.py avec Visual studio et **l'éxecuter** puis suivre les étapes indiquées
